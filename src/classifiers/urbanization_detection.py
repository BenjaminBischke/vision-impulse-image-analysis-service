#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions of the urbanization classifier.
"""

from classifiers.models.resnet50 import resnet50
from classifiers.transforms import default_val_transforms
from torch.autograd import Variable
import torch
from PIL import Image
import os
from config import MODEL_DIR


class UrbanizationDetectionClassifier(object):
    """
        This class wraps a CNN model (ResNet50) for the multi-class classification of urban density.
    """

    def __init__(self, logger):
        self.logger = logger
        path = os.path.join(MODEL_DIR, "urbanization.model_best.pth.tar")
        self.network = resnet50(pretrained=False, num_classes=3)
        storage_device = torch.device("cpu")
        if torch.cuda.is_available():
            self.network.cuda()
            storage_device = None
        snapshot = torch.load(path, map_location=storage_device)
        model_state = snapshot.pop("state_dict", snapshot)
        model_state = dict(
            (k.replace("module.", ""), v)
            for k, v in model_state.items()
            if not "num_batches_tracked" in k
        )
        sd = self.network.state_dict()
        sd.update(model_state)
        self.network.load_state_dict(sd)
        self.network.eval()
        self.transform = default_val_transforms()

    def urbanization_level(self, image_fp):
        """
        This function runs the urbanization classifier for a given image path.

        :param image_fp: file path of the image
        :return: urbanization level (string: low, medium, high)
        """
        self.logger.info(" ==> Running urbanization classifier... " + image_fp)
        img = Image.open(image_fp).convert("RGB")
        img = self.transform(img)

        img = Variable(img.float()).unsqueeze(0)
        if torch.cuda.is_available():
            img = img.cuda()

        output = self.network(img)
        output = output.data

        if torch.cuda.is_available():
            output = output.cpu()

        level = output.numpy().argmax()
        if level == 0:
            return "low"
        if level == 1:
            return "medium"
        return "high"
