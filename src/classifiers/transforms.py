# -*- coding: iso-8859-15 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for image transformations.
"""

import torchvision.transforms as t_transforms
import torch


# Default augmentation values compatible with ImageNet data augmentation pipeline
_DEFAULT_ALPHASTD = 0.1
_DEFAULT_EIGVAL = [0.2175, 0.0188, 0.0045]
_DEFAULT_EIGVEC = [
    [-0.5675, 0.7192, 0.4009],
    [-0.5808, -0.0045, -0.8140],
    [-0.5836, -0.6948, 0.4203],
]
_DEFAULT_BCS = [0.4, 0.4, 0.4]

NORMALIZE = t_transforms.Normalize(
    mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
)


def default_val_transforms():
    """
    This function contains the default transformation for the images.

    :returns: a list of default image transformations that have been also used in the training
    """
    val_transforms = [t_transforms.Resize(256)]
    val_transforms += [
        t_transforms.CenterCrop(224),
        t_transforms.ToTensor(),
    ]
    val_transforms.append(Lighting(alphastd=0.0))
    val_transforms.append(NORMALIZE)
    val_transforms = t_transforms.Compose(val_transforms)
    return val_transforms


def _grayscale(img):
    alpha = img.new([0.299, 0.587, 0.114])
    return (alpha.view(3, 1, 1) * img).sum(0, keepdim=True)


def _blend(img1, img2, alpha):
    return img1 * alpha + (1 - alpha) * img2


class Lighting:
    """
    This class contains the lighting transformation for the image.
    """

    def __init__(
        self, alphastd=_DEFAULT_ALPHASTD, eigval=_DEFAULT_EIGVAL, eigvec=_DEFAULT_EIGVEC
    ):
        self._alphastd = alphastd
        self._eigval = eigval
        self._eigvec = eigvec

    def __call__(self, img):
        if self._alphastd == 0.0:
            alpha = img.new_zeros(3)
        else:
            alpha = torch.normal(img.new_zeros(3), self._alphastd)

        eigval = img.new(self._eigval)
        eigvec = img.new(self._eigvec)
        rgb = (eigvec * alpha * eigval).sum(dim=1)
        return img + rgb.view(3, 1, 1)


class Saturation(object):
    """
    This class contains the saturation transformation for the image.
    """

    def __init__(self, var):
        self._var = var

    def __call__(self, img):
        gs = _grayscale(img)
        alpha = img.new(1).uniform_(-self._var, self._var) + 1.0
        return _blend(img, gs, alpha)


class Brightness(object):
    """
    This class contains the brightness transformation for the image.
    """

    def __init__(self, var):
        self._var = var

    def __call__(self, img):
        gs = torch.zeros_like(img)
        alpha = img.new(1).uniform_(-self._var, self._var) + 1.0
        return _blend(img, gs, alpha)


class Contrast(object):
    """
    This class contains the contrast transformation for the image.
    """

    def __init__(self, var):
        self._var = var

    def __call__(self, img):
        gs = _grayscale(img)
        gs = img.new_full((1, 1, 1), gs.mean())
        alpha = img.new(1).uniform_(-self._var, self._var) + 1.0
        return _blend(img, gs, alpha)
