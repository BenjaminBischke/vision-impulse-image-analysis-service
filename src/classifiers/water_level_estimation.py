#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions of the water level estimation classifier.
"""

#from libs.object_detection import ObjectDetector
from config import MODEL_DIR, PROJECT_DIR
import os
import cv2
from libs.openpose.body import Body
import numpy as np
from operator import itemgetter
from sklearn.externals import joblib
from PIL import Image
from classifiers.water_patch_detection import WaterPatchClassifier


class WaterLevelEstimationClassifier(object):
    """
        This class wraps a CNN model (ResNet50) as well as human pose estimation to determine the water level.
    """

    _open_pose_model = None
    _open_pose_feature_svc = None
    _open_pose_feature_scaler = None
    _waterpatch_model = None

    def __init__(self, logger):
        self.logger = logger

    @property
    def waterpatch_model(self):
        """
        :return: the model for water patch classification (ResNet18)
        """
        if self._waterpatch_model is None:
            self._waterpatch_model = WaterPatchClassifier(self.logger)
        return self._waterpatch_model

    @property
    def open_pose_model(self):
        """
        :return: the model for pose estimation (OpenPose)
        """
        if self._open_pose_model is None:
            self._open_pose_model = Body(os.path.join(MODEL_DIR, "body_pose_model.pth"))
        return self._open_pose_model

    @property
    def open_pose_feature_svc(self):
        """
        :return: support vector machine classifier to classify the open pose feature vector
        """
        if self._open_pose_feature_svc is None:
            self._open_pose_feature_svc = joblib.load(
                os.path.join(MODEL_DIR, "openpose_svc.sav")
            )
        return self._open_pose_feature_svc

    @property
    def open_pose_feature_scaler(self):
        """
        :return: scaler to normalize the open pose feature vector
        """
        if self._open_pose_feature_scaler is None:
            self._open_pose_feature_scaler = joblib.load(
                os.path.join(MODEL_DIR, "openpose_scaler.sav")
            )
        return self._open_pose_feature_scaler

    def has_person_depth_evidence(self, image_fp, obj_detections):
        """ This function determines whether the water level in the image is above or below knee level of a person.

        :param image_fp: file path to the image

        :param obj_detections: object detections from Faster-RCNN

        :return: True if the water level is above knee level otherwise False
        """
        self.logger.info(" ==> Running water level classifier... " + image_fp)
        img = cv2.imread(image_fp)
        if img is None:
            return False

        detections = [(i["bbox"][2]-i["bbox"][0], i) for i in obj_detections if i["object_class"] == "person"]
        detections = sorted(detections, key=itemgetter(0), reverse=True)

        for jdx, (_, detection) in enumerate(detections):
            img = cv2.imread(image_fp)
            crop, x2, y2 = self._crop_person(img, detection)

            x1, y1, x2, y2 = detection["bbox"]

            img_h, img_w, img_c = img.shape
            if abs(y2 - img_h) < 10:
                continue
            if abs(x2 - img_w) < 10:
                continue

            vec = self._run_open_pose(crop)

            if self._classify_pose(vec):
                x1, y1, x2, y2 = detection["bbox"]
                h = (y2 - y1)
                foot_crop2 = img[int(y2) - int(h * 0.3): int(y2), int((x1)): int((x2)), :]
                foot_crop_img2 = Image.fromarray(foot_crop2).convert("RGB")

                if self.waterpatch_model.is_water_patch(foot_crop_img2):
                    return True
        return False
        """
        for detection in [i for i in obj_detections if i["object_class"] == "person"]:
            crop = self._crop_person(img, detection)
            op_feature = self._run_open_pose(crop)
            if self._classify_pose(op_feature):
                return True
        return False
        """

    def _crop_person(self, img, detection):
        x1, y1, x2, y2 = detection["bbox"]
        return img[int(y1) : int(y2), int(x1) : int(x2), :], x2, y2

    def _run_open_pose(self, crop):
        h, w, c = crop.shape
        candidate, subset = self.open_pose_model(crop)

        op_features = []
        for person_idx in range(len(subset)):
            feature = []
            for p in subset[person_idx][:18]:
                if p == -1:
                    val = np.array([-1.0, -1.0, -1.0,])
                else:
                    val = np.array(
                        [
                            float(candidate[int(p)][0]) / w,
                            float(candidate[int(p)][1]) / h,
                            candidate[int(p)][2],
                        ]
                    )
                feature.append(val)

            feature = list(np.array(feature).flatten())
            num_pose_points = subset[person_idx][18]

            if feature[4] == -1.0:
                x_position = 0
            else:
                x_position = abs(w / 2 - feature[4])
            op_features.append((x_position, num_pose_points, feature))

        # select the most centered person in case that there are more persons
        # visible in the cropped patch
        op_features = sorted(op_features, key=itemgetter(0))

        if len(subset) == 0:
            open_pose_vec = np.array([-1.0] * 54)  # 18 * 3
        else:
            open_pose_vec = np.array(op_features[0][2]).flatten()
        return open_pose_vec

    def _classify_pose(self, op_feature):
        X = np.array([op_feature])
        X = self.open_pose_feature_scaler.transform(X)
        X_pred = self.open_pose_feature_svc.predict(X)
        return X_pred[0] == 1
