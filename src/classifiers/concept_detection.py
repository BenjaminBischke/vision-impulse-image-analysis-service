#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for the concept detection.
"""

from torch import nn


from libs.object_detection import ObjectDetector
from classifiers.response_worker_detection import ResponseWorkerDetectionClassifier
from config import MODEL_DIR, PROJECT_DIR
import os


class ConceptDetectionClassifier(object):
    """
    This class wraps a Faster-RCNN model (with ResNet101 as backbone) for the binary
    classification for flood related images.
    """

    _obj_detector = None
    _response_detector = None

    def __init__(self, logger):
        self.logger = logger
        self.res = None

    @property
    def obj_detector(self):
        """
        :return: the model of the object detector (Faster-RCNN model with ResNet101 as backbone).
        """
        if self._obj_detector is None:
            model_path = os.path.join(MODEL_DIR, "faster_rcnn_1_10_625.pth")
            cfg_path = os.path.join(
                PROJECT_DIR, "libs", "faster_rcnn", "cfgs", "res101.yml"
            )
            self._obj_detector = ObjectDetector(model_path, cfg_path)
        return self._obj_detector

    @property
    def response_detector(self):
        """
        :return: the model of the response worker detector.
        """
        if self._response_detector is None:
            self._response_detector = ResponseWorkerDetectionClassifier(self.logger)
        return self._response_detector

    def analyse_image(self, image_fp):
        """
        Performs the concept detection for a given image_path and holds the result in a class member.

        :param image_fp (string): path to the downloaded image
        """
        self.logger.info(" ==> Running concept detection classifier... " + image_fp)
        self.res = self.obj_detector.analyze_image(image_fp)

    def contains_concepts(self, concepts):
        """
        This functions determines whether the image depicts at least one object with the specific concept class.

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :param concepts: the name of the concepts
        :return: True/False if there is a concept depicted.
        """
        for i in self.res:
            if i["object_class"] in concepts:
                return True
        return False

    def is_person_present(self):
        """
        This functions determines whether the image depicts one or more persons.

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a person.
        """
        return self.contains_concepts(["person",])

    def is_cattle_present(self):
        """
        This functions determines whether the image depicts one or more cattles (cows, sheeps, horses).

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a cattle.
        """
        return self.contains_concepts(["horse", "sheep", "cow",])

    def is_motorized_transport_present(self):
        """
        This functions determines whether the image depicts one or more motorized transport vehicles (bus, car, motorbike).

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a motorized transport vehicle.
        """
        return self.contains_concepts(
            ["bus", "car", "motorbike", "aeroplane", "train",]
        )

    def is_non_motorized_transport_present(self):
        """
        This functions determines whether the image depicts one or more non motorized transport vehicles (bicycle).

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a non motorized transport vehicle.
        """
        return self.contains_concepts(["bicycle",])

    def is_water_transport_present(self):
        """
        This functions determines whether the image depicts one or more water transport vehicles (boat).

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a water transport vehicle.
        """
        return self.contains_concepts(["boat",])


    def is_response_worker_present(self, img_path):
        """
        This functions determines whether the image depicts one or more response workers.

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a response worker.
        """
        return self.response_detector.response_worker_present(img_path, self.res)

    def is_building_present(self):
        """
        This functions determines whether the image depicts one or more buildings.

        .. note::
            Make sure that you run the method 'analyse_image' before this function!

        :return: True/False if there is/is not a building.
        """

        return True
