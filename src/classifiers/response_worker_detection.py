#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions of the response worker classifier.
"""

from classifiers.models.resnet50 import resnet50
from classifiers.transforms import default_val_transforms
from torch.autograd import Variable
import torch
from PIL import Image
import os
from config import MODEL_DIR
import numpy as np
import cv2


class ResponseWorkerDetectionClassifier(object):
    """
        This class wraps a CNN model () for the detection of rescue workers in images.
    """

    _network = None
    _transform = None

    def __init__(self, logger):
        self.logger = logger

    @property
    def network(self):
        """
        :return: a pre-trained ResNet50 model for the image analysis
        """
        if self._network is None:
            path = os.path.join(MODEL_DIR, "rescuer.model_best.pth.tar")
            self._network = resnet50(pretrained=False, num_classes=1)
            storage_device = torch.device("cpu")
            if torch.cuda.is_available():
                self._network.cuda()
                storage_device = None
            snapshot = torch.load(path, map_location=storage_device)
            model_state = snapshot.pop("state_dict", snapshot)
            model_state = dict(
                (k.replace("module.", ""), v)
                for k, v in model_state.items()
                if not "num_batches_tracked" in k
            )
            sd = self._network.state_dict()
            sd.update(model_state)
            self._network.load_state_dict(sd)
            self._network.eval()
        return self._network

    @property
    def transform(self):
        """
        :return: default transforms for the image pre-processing before feeding it to the CNN
        """
        if self._transform is None:
            self._transform = default_val_transforms()
        return self._transform


    def response_worker_present(self, image_fp, obj_detections):
        """
        This function runs the rescue worker detection for a given image path.

        :param image_fp: file path to the image

        :param obj_detections: object detections from Faster-RCNN

        :return: True if rescue worker present in image otherwise False
        """

        self.logger.info(" ==> Running response worker detection classifier... " + image_fp)
        img = cv2.imread(image_fp)

        for detection in [i for i in obj_detections if i["object_class"] == "person"]:
            crop_, x2, y2 = self._crop_person(img, detection)
            crop = Image.fromarray(crop_).convert("RGB")

            crop = self.transform(crop)
            crop = Variable(crop).float().unsqueeze(0)
            if torch.cuda.is_available():
                crop = crop.cuda()

            output = self.network(crop)
            pred = torch.sigmoid(output.float())
            pred = pred.t()
            pred[pred < 0.5] = 0
            pred[pred >= 0.5] = 1

            data = pred[0][0]
            if torch.cuda.is_available():
                data = data.cpu()
            if bool(data.detach().numpy() == 1):
                return True
        return False

    def _crop_person(self, img, detection):
        x1, y1, x2, y2 = detection["bbox"]
        return img[int(y1):int(y2), int(x1):int(x2), :], x2, y2




