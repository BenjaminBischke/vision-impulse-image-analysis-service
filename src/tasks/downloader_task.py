#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for downloading the images.
"""

import os
import requests
import shutil
import os.path as op
import io
from PIL import Image
from functools import wraps
from dateutil.parser import parse as dateparse
from urllib.parse import urlparse, urlunparse
import time
from tasks.downloader_database import ImageDownloaderDatabase
from config import DATA_FOLDER
from celery_message import TagAnalysisMessage
import urllib3
import imagehash

import logging
logger = logging.getLogger(__name__)


class ImageDownloaderException(Exception):
    """
    This class represents a custom Exception for this module.
    """
    pass


def retry(exceptions, tries=1, delay=2, backoff=2):
    """
    This function retries the downloading in case there was an error.

    :param exceptions: the exceptions that is catched in the retry
    """
    def deco_retry(f):
        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except exceptions as e:
                    msg = "{}, Retrying in {} seconds...".format(e, mdelay)
                    print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry


class ImageDownloaderTask(object):
    """
    This class is responsible for downloading all images for a given Tag.
    """
    name = "ImageDownloaderTask"

    def __init__(self):
        super(ImageDownloaderTask, self).__init__()
        self.logger = logger
        self.database = ImageDownloaderDatabase()

    @staticmethod
    def _image_path_from_url(
        newspaper, year, image_id, img_url, root_folder=DATA_FOLDER
    ):
        img_url_clean = urlunparse(urlparse(img_url)._replace(query="", fragment=""))
        suffix = ".jpg" if img_url_clean.endswith(".jpg") else ".png"
        return os.path.join(root_folder, newspaper, year, str(image_id) + suffix)

    @staticmethod
    def _is_image_truncated(path):
        try:
            with io.FileIO(path) as f:
                data = f.read()
                buf = io.BytesIO(data)
                Image.open(buf).load()
                return False
        except OSError as err:
            return True

    @staticmethod
    def _image_hash(path):
        return str(imagehash.phash(Image.open(path)))

    @staticmethod
    def _download_image(img_url, image_path):
        print("Downloading: ", img_url)
        try:
            response = requests.get(img_url, stream=True, timeout=5)
            success = response.status_code == 200
            if success:
                with open(image_path, "wb") as out_file:
                    response.raw.decode_content = True
                    shutil.copyfileobj(response.raw, out_file)
                response._content_consumed = True
                return True
            else:
                return False
        except urllib3.exceptions.NewConnectionError as err:
            return False
        except urllib3.exceptions.ReadTimeoutError as err:
            return False

    def download_and_validate_image(self, img_url, image_path):
        """
        This function downloads and validates the images

        :param img_url: the URL of the image resource
        :param image_path: the destination path to which the image should be written on the filesystem
        :return: the status (True/False) if downloading and verification (image truncation checks) was successful
        """
        if not self._download_image(img_url, image_path):
            return False
        if self._is_image_truncated(image_path):
            return False
        else:
            return True

    def run(self, tag_analysis_msg):
        """
        This function takes the internal message object and downloads all images of a tag.

        :param message: the internal message object
        :return: the message object with the paths to downloaded images
        """
        message = TagAnalysisMessage.from_json(tag_analysis_msg)
        message.img_download_results = self.download_images(message.original_tag)
        return message

    def download_images(self, tag):
        """
        This function takes the (FloodTags) Tag object and downloads the images.

        :param tag: the Tag object
        :return: a list with meta information of the downloaded images (status, path, url, duplicate)
        """
        year = str(dateparse(tag["date"]).year)
        news_article_name = tag["id"].split("-")[1]
        result_downloader = []
        try:
            for idx_photo, tag_photo in enumerate(tag["media"]["photos"]):
                image_id = None
                try:
                    image_id = tag["source"]["id"] + "_" + str(idx_photo)
                    image_path = self._image_path_from_url(
                        news_article_name, year, image_id, tag_photo["url"]
                    )
                    if not os.path.exists(os.path.dirname(image_path)):
                        os.makedirs(os.path.dirname(image_path))
                    image_duplicate_path = self.database.select_path_for_url(
                        tag_photo["url"]
                    )
                    if image_duplicate_path is None:
                        url_duplicate = False
                    else:
                        url_duplicate = True
                        image_path = image_duplicate_path

                    hash_duplicate = False
                    if not os.path.exists(image_path):
                        task_success = self.download_and_validate_image(
                            tag_photo["url"], image_path
                        )
                        if task_success:
                            task_success = "Successful"
                            if not url_duplicate:
                                self.database.insert_url_and_image_path(
                                    tag_photo["url"], image_path
                                )
                                image_hash = self._image_hash(image_path)
                                if self.database.select_md5_url_for_image_hash(image_hash) is None:
                                    self.database.insert_image_hash(tag_photo["url"], image_hash)
                                else:
                                    hash_duplicate = True

                        else:
                            task_success = "Failed"
                            image_path = ""
                    else:
                        task_success = "Successful"

                    duplicate = url_duplicate or hash_duplicate
                    res = {
                        "path": image_path,
                        "url": tag_photo["url"],
                        "status": task_success,
                        "duplicate": duplicate,
                    }
                    result_downloader.append(res)
                except Exception as exp:
                    if image_id is not None:
                        self.logger.exception(
                            "Image Download for %s failed (%s)" % (image_id, exp)
                        )
            return result_downloader
        except Exception as exp:
            self.logger.exception("Image Download failed (%s)" % exp)
            return [{}]
