#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for the internal image database.
"""

import logging

import mysql.connector as mc
from urllib.parse import urlparse, urlunparse
import hashlib
from config import DB_HOST
from config import DB_USER_NAME
from config import DB_USER_PWD
from config import DB_DATABASE_NAME

logger = logging.getLogger(__name__)


class ImageDownloaderDatabase(object):
    """
    This class represents the database object and holds the connection to the image database.
    It is used for the near duplicate detection based on URLs.
    """
    def __init__(
        self,
        host=DB_HOST,
        user=DB_USER_NAME,
        passwd=DB_USER_PWD,
        database=DB_DATABASE_NAME,
    ):
        self.logger = logger
        self.host = host
        self.user = user
        self.passwd = passwd
        self.database = database
        self.table_name_downloader = "image_downloader"
        self.table_name_duplicate_detection = "image_duplicate_detection"
        self.connection = self._get_connection_to_db()

    def select_path_for_url(self, image_url):
        """
        This function selects the distinct path from the internal database for a given image_url.

        :param image_url: the image URL which is selected in the table

        :returns the file path to an already downloaded resource or None in case that the image has not been seen beforehand
        """
        sql_stmt = """SELECT DISTINCT path FROM {table_name} 
            WHERE md5_url = '{md5_url}' """
        sanitized_url = self._sanitize_url(image_url)
        md5_hash = hashlib.md5(sanitized_url.encode()).hexdigest()
        sql_stmt = sql_stmt.format(table_name=self.table_name_downloader, md5_url=md5_hash)
        all_paths = self._execute_sql_stmt(sql_stmt)
        if len(all_paths) > 0:
            return all_paths[0][0]
        return None

    def select_md5_url_for_image_hash(self, image_hash):
        """
        This function selects the distinct path from the internal database for a given image_hash.

        :param image_url: the image hash value which is selected in the table

        :returns the file path to an already downloaded resource or None in case that the image has not been seen beforehand
        """
        sql_stmt = """SELECT DISTINCT md5_url FROM {table_name} 
            WHERE image_hash = '{image_hash}' """
        sql_stmt = sql_stmt.format(table_name=self.table_name_duplicate_detection, image_hash=image_hash)
        all_paths = self._execute_sql_stmt(sql_stmt)
        if len(all_paths) > 0:
            return all_paths[0][0]
        return None

    def insert_url_and_image_path(self, image_url, image_path):
        """
        This function inserts a file path and the url of an image into the internal database.

        :param image_url: the URL of the image

        :param image_path: the file path of the image
        """
        sql_stmt = """INSERT INTO {table_name} (original_url, sanitized_url, md5_url, path)
            VALUES ('{original_url}', '{sanitized_url}', '{md5_url}', '{path}');"""

        sanitized_url = self._sanitize_url(image_url)
        md5_hash = hashlib.md5(sanitized_url.encode()).hexdigest()
        sql_stmt = sql_stmt.format(
            table_name=self.table_name_downloader,
            original_url=image_url,
            sanitized_url=sanitized_url,
            md5_url=md5_hash,
            path=image_path,
        )
        self._execute_sql_stmt(sql_stmt)

    def insert_image_hash(self, image_url, image_hash):
        """
        This function inserts the hash of an image into the internal database for future duplicate detection.

        :param image_url: the URL of the image

        :param image_hash: the hash value of the image
        """
        sql_stmt = """INSERT INTO {table_name} (md5_url, image_hash)
            VALUES ('{md5_url}', '{image_hash}');"""

        sanitized_url = self._sanitize_url(image_url)
        md5_hash = hashlib.md5(sanitized_url.encode()).hexdigest()
        sql_stmt = sql_stmt.format(
            table_name=self.table_name_duplicate_detection,
            md5_url=md5_hash,
            image_hash=image_hash,
        )
        self._execute_sql_stmt(sql_stmt)

    def _sanitize_url(self, url):
        return urlunparse(urlparse(url)._replace(query="", fragment=""))

    def _get_connection_to_db(self):  # cache db handle
        if not hasattr(self, "connection") or self.connection is None:
            self.connection = mc.connect(
                host=self.host, user=self.user, passwd=self.passwd, db=self.database
            )
        return self.connection

    def _close_connection_to_db(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def _execute_sql_stmt(self, sql_command):
        try:
            connection = self._get_connection_to_db()
            cursor = connection.cursor()
            cursor.execute(sql_command)
            res = [row for row in cursor]
            connection.commit()
            cursor.close()
            return res
        except Exception as exp:
            self._close_connection_to_db()
            self.logger.exception(
                "Failed to execute Cursor on Downloader Database %s" % (exp)
            )

    def _server_version(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT VERSION()")
        row = cursor.fetchone()
        print("server version:", row[0])
        cursor.close()
