#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for the actual image analysis.
"""

import logging

from config import ANALYSIS_VERSION
from classifiers import (
    concept_detection,
    flood_detection,
    urbanization_detection,
    water_level_estimation,
)
from datetime import datetime

logger = logging.getLogger(__name__)


class ImageAnalysisTask(object):
    """
    This class is responsible for executing the logic of the image analysis.
    The analysis relies on multiple pre-trained CNN Models.
    """

    name = "ImageAnalysisTask"

    _flood_detection_cls = None
    _concept_detection_cls = None
    _water_level_estimation_cls = None
    _urbanization_detection_cls = None

    @property
    def flood_detection_cls(self):
        """
        :return: the flood detection (binary) classifier
        """
        if self._flood_detection_cls is None:
            self._flood_detection_cls = flood_detection.FloodDetectionClassifier(
                self.logger
            )
        return self._flood_detection_cls

    @property
    def concept_detection_cls(self):
        """
        :return: the object detection model (Faster-RCNN)
        """
        if self._concept_detection_cls is None:
            self._concept_detection_cls = concept_detection.ConceptDetectionClassifier(
                self.logger
            )
        return self._concept_detection_cls

    @property
    def waterlevel_estimation_cls(self):
        """
        :return: the water level estimation classifier (binary above/blow knee level)
        """
        if self._water_level_estimation_cls is None:
            self._water_level_estimation_cls = water_level_estimation.WaterLevelEstimationClassifier(
                self.logger
            )
        return self._water_level_estimation_cls

    @property
    def urbanization_detection_cls(self):
        """
        :return: the urbanisation detection classifier (low, medium, high density)
        """
        if self._urbanization_detection_cls is None:
            self._urbanization_detection_cls = urbanization_detection.UrbanizationDetectionClassifier(
                self.logger
            )
        return self._urbanization_detection_cls

    def __init__(self):
        super(ImageAnalysisTask, self).__init__()
        self.logger = logger

    def run(self, message):
        """
        This function takes the internal message object and runs the image analysis.

        :param message: the internal message object

        :return: the message object with the image analysis result
        """
        img_result = self._process_images(
            message.original_tag, message.img_download_results
        )
        message.img_analysis_results = img_result
        return message

    def _process_images(self, tag, img_download_results):
        """
        This functions performs the main image analysis. (internal)

        :param tag: the (FloodTags) Tag object

        :param img_download_results: the output of the image downloader task

        :return: a list containing image analysis results for each image
        """
        analysis_results = []

        for tag_image, download_info in list(
            zip(tag["media"]["photos"], img_download_results)
        ):
            img_path = download_info.get("path", "")

            if img_path == "":
                timestamp = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
                image_results = {
                    "analysisTimestamp": timestamp,
                    "analysisVersion": str(ANALYSIS_VERSION),
                    "floodRelated": False,
                }
                analysis_results.append(image_results)  # missing img
                continue

            timestamp = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
            is_flood_related = self.flood_detection_cls.is_flood_related(img_path)
            image_results = {
                "analysisTimestamp": timestamp,
                "analysisVersion": str(ANALYSIS_VERSION),
                "floodRelated": is_flood_related,
            }

            if is_flood_related:
                self.concept_detection_cls.analyse_image(img_path)

                is_cattle_present = self.concept_detection_cls.is_cattle_present()
                is_motorized_transp_present = (
                    self.concept_detection_cls.is_motorized_transport_present()
                )
                is_water_transp_present = (
                    self.concept_detection_cls.is_water_transport_present()
                )
                is_non_motorized_transp_present = (
                    self.concept_detection_cls.is_non_motorized_transport_present()
                )
                is_building_present = self.concept_detection_cls.is_building_present()
                is_person_present = self.concept_detection_cls.is_person_present()

                image_results.update(
                    {
                        "cattlePresent": is_cattle_present,
                        "transportMotorizedPresent": is_motorized_transp_present,
                        "transportWaterPresent": is_water_transp_present,
                        "transportNonMotorizedPresent": is_non_motorized_transp_present,
                        "buildingPresent": is_building_present,
                        "personPresent": is_person_present,
                    }
                )

                if is_building_present:
                    urbanization_level = self.urbanization_detection_cls.urbanization_level(
                        img_path
                    )
                    image_results.update({"urbanization": urbanization_level})

                if is_person_present:
                    is_response_worker_present = (
                        self.concept_detection_cls.is_response_worker_present(img_path)
                    )
                    has_person_depth_evidence = self.waterlevel_estimation_cls.has_person_depth_evidence(
                        img_path, self.concept_detection_cls.res
                    )
                    image_results.update(
                        {
                            "personDepthEvidence": has_person_depth_evidence,
                            "responseWorkerPresent": is_response_worker_present,
                        }
                    )

            analysis_results.append(image_results)
        return analysis_results
