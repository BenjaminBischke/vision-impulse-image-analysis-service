#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for sending the results to the FloodTags system.
"""

from floodtags_sdk import adapter


class ResultSenderTask(object):
    """
    This class is responsible for composing the result message of the image analysis and sending it to the adapter.
    """

    name = "ResultSenderTask"

    def __init__(self):
        super(ResultSenderTask, self).__init__()

    @staticmethod
    def construct_analysis_result(message):
        """
        This functions constructs the final message to be send by the adapter.

        :param message: the internal message object
        :return: the constructed final message
        """
        payload = {"type": "tag", "id": message.original_tag["id"], "operations": []}
        image_captions = [
            img.get("caption", "") for img in message.original_tag["media"]["photos"]
        ]
        payload_value = []
        for analysis_res, download_res, caption in list(
            zip(
                message.img_analysis_results,
                message.img_download_results,
                image_captions,
            )
        ):
            if len(analysis_res) > 0:
                analysis_res.update(
                    {"imagePreviouslyUsed": bool(download_res["duplicate"])}
                )
            payload_value.append(
                {
                    "url": download_res["url"],
                    "caption": caption,
                    "analysisResults": analysis_res,
                }
            )

        payload_operations = [
            {"type": "replace", "key": "analyzedPhotos", "value": payload_value}
        ]

        payload.update({"operations": payload_operations})
        return payload

    def run(self, message):
        """
        This function takes the internal message object and sends the analysis results back to the adapter.

        :param message: the internal message object
        """
        result = self.construct_analysis_result(message)
        adapter.send(result, "flood-image-analysis")
