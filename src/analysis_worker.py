#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions of the worker executing the computational tasks.
"""

from config import CELERY_BROKER_DATA_FOLDER_IN, CELERY_BROKER_DATA_FOLDER_PROCESSED
from tasks.downloader_task import ImageDownloaderTask
from tasks.image_analysis_task import ImageAnalysisTask
from tasks.sender_task import ResultSenderTask


import os
import time
from multiprocessing import Process

import json
import shutil
import glob

from floodtags_sdk import logging

# This get's and configures the root logger for this script. Even though it's
# not called here, any loggers in packages and submodules will propagate to
# this and it makes sure Critical errors are logged before exit
logger = logging.get_logger('flood-image-analysis-worker')

from floodtags_sdk.formats import Tag
import json
from config import POLLING_TIME
from config import NUM_TAGS_TO_PROCESS


class TagAnalysisMessage(object):
    """
    This class represents an internal data-structure to hold the tag object and the analysis results.
    This internal message object is passed between the tasks.
    """

    def __init__(
        self, tag_dict, img_download_results, img_analysis_results, tag_version="1.1"
    ):
        super(TagAnalysisMessage, self).__init__()
        self.original_tag = Tag.from_dict(tag_dict, as_version=tag_version)
        self.img_download_results = img_download_results
        self.img_analysis_results = img_analysis_results
        pass

    @staticmethod
    def from_json(json_msg):
        """
        This function converts a json message to a 'TagAnalysisMessage' object.

        :param json_msg: the message as json

        :return: the message object
        """
        msg = json.loads(json_msg)
        return TagAnalysisMessage(
            json.loads(msg["original_tag"]),
            json.loads(msg["img_download_results"]),
            json.loads(msg["img_analysis_results"]),
        )

    def to_json(self):
        """
        This function converts a 'TagAnalysisMessage' object to a json message.

        :return: the message object as json
        """
        payload = {
            "original_tag": json.dumps(Tag.to_dict(self.original_tag)),
            "img_analysis_results": json.dumps(self.img_analysis_results),
            "img_download_results": json.dumps(self.img_download_results),
        }
        return json.dumps(payload, ensure_ascii=False)


def process_message(files):
    """
    This function performs the steps in the analysis pipeline (download images, run the image analysis, send result)

    :param files: The files to be analyzed
    """
    img_downloader_task = ImageDownloaderTask()
    img_analysis_task = ImageAnalysisTask()
    sender_task = ResultSenderTask()

    for trigger_file_name in files:
        json_fn = trigger_file_name.replace(".trigger", ".json")
        print("Processing Tag ", json_fn, "...")
        trigger_file_path = os.path.join(
            CELERY_BROKER_DATA_FOLDER_IN, trigger_file_name
        )
        json_file_path = os.path.join(CELERY_BROKER_DATA_FOLDER_IN, json_fn)
        try:
            with open(json_file_path, "r") as fp:
                json_tag = json.load(fp)
                result = img_downloader_task.run(json_tag)
                result = img_analysis_task.run(result)
                sender_task.run(result)

                os.remove(json_file_path)
                os.remove(trigger_file_path)
        except Exception as ex:
            shutil.move(
                trigger_file_path, trigger_file_path.replace(".trigger", ".err")
            )


class AnalysisWorkerTask(object):
    """
    This class is responsible for polling new tags to be analyzed from the filesystem and spawning new processes for the analysis.
    """

    name = "AnalysisWorkerTask"

    def __init__(self):
        super(AnalysisWorkerTask, self).__init__()
        self.num_tags = NUM_TAGS_TO_PROCESS

    def run(self):
        """
        Main function to start the event loop
        """
        while 1:
            files = glob.glob(os.path.join(CELERY_BROKER_DATA_FOLDER_IN, "*.trigger"))
            if len(files) == 0:
                time.sleep(POLLING_TIME)
                continue

            for idx in range(0, len(files), self.num_tags):
                _files = files[idx : idx + self.num_tags]
                p = Process(target=process_message, args=(_files,))
                p.start()
                p.join()


if __name__ == "__main__":
    try:
        task = AnalysisWorkerTask()
        task.run()
    except KeyboardInterrupt:
        pass
    finally:
        print()
