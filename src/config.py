# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for the configuration of the project.
"""

import os
import inspect
import yaml
from pathlib import Path


def load_config(filedir, filename):
    """
    This function parses and loads the configuration files from Vision Impulse.

    :param filedir: directory in which the config file is located

    :param filename: filename of the config file

    :return: a dictionary with the config entries as key-value pairs
    """
    config_file_loc = Path(filedir, filename)
    if not config_file_loc.is_file():
        print("Can't find local config file:", config_file_loc)
        return
    with open(os.path.join(filedir, filename), "r", encoding="utf8") as configfile:
        config_data = yaml.safe_load(configfile)
    return config_data


PROJECT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
config_dir = os.path.join(PROJECT_DIR, "../data", "configs")
os.environ["CONFIG_DIR"] = config_dir

vi_config = load_config(config_dir, "vision_impulse.yaml")

ANALYSIS_VERSION = vi_config["analysis"]["version"]
MODEL_DIR = os.path.expanduser(vi_config["dir"]["model"])
DATA_FOLDER = os.path.expanduser(vi_config["dir"]["data"])

DB_HOST = vi_config["database"]["host"]
DB_USER_NAME = vi_config["database"]["username"]
DB_USER_PWD = vi_config["database"]["password"]
DB_DATABASE_NAME = vi_config["database"]["db_name"]

CELERY_BROKER_URL = vi_config["celery"]["broker_url"]
CELERY_BROKER_DATA_FOLDER_OUT = os.path.expanduser(
    vi_config["celery"]["broker_out_dir"]
)
CELERY_BROKER_DATA_FOLDER_IN = os.path.expanduser(vi_config["celery"]["broker_in_dir"])
CELERY_BROKER_DATA_FOLDER_PROCESSED = os.path.expanduser(
    vi_config["celery"]["broker_processed_dir"]
)

NUM_TAGS_TO_PROCESS = vi_config["celery"]["num_tags_to_process"]
POLLING_TIME = vi_config["celery"]["polling_time"]

CELERY_TASK_SERIALIZER = vi_config["celery"]["task_serializer"]
CELERY_RESULT_SERIALIZER = vi_config["celery"]["result_serializer"]
CELERY_RESULT_PERSISTENT = vi_config["celery"]["result_persistent"]
CELERY_ACCEPT_CONTENT = vi_config["celery"]["accept_content"]
CELERY_TIMEZONE = vi_config["celery"]["timezone"]
CELERY_ENABLE_UTC = vi_config["celery"]["enable_utc"]

FLASK_HOST = vi_config["flask"]["host"]
FLASK_DEBUG = vi_config["flask"]["debug"]
