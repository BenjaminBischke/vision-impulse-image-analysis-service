# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This script removes images in data folder if the number of kept files exceeds a given limit
(removed sorted by date, first-in first-out).
"""

from config import PROJECT_DIR, config_dir, load_config
from glob import glob
from pathlib import Path
import os



def remove_old_images(file_limit, vi_data_folder):
    """
    This function removes old images in the given folder and keeps the recent number of images as defined in 'file_limit'
    :param file_limit: Number of images to keep
    :param vi_data_folder: Path where the project data is stored
    """
    img_files = glob(os.path.join(os.path.expanduser(vi_data_folder), '*', '*', '*.*'))
    count = len(img_files)
    if count > file_limit:
        img_files.sort(key=os.path.getmtime)
        img_files = img_files[:count - file_limit]
        for img in img_files:
            try:
                os.remove(img)
            except:
                pass


if __name__ == "__main__":
    FILE_LIMIT = 5000
    vi_config = load_config(config_dir, "vision_impulse.yaml")
    os.environ["HOME"] = os.path.expanduser("~" + Path(PROJECT_DIR).owner())
    remove_old_images(FILE_LIMIT, vi_config["dir"]["data"])
