#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

from floodtags_sdk.formats import Tag
import json


class TagAnalysisMessage(object):
    def __init__(
        self, tag_dict, img_download_results, img_analysis_results, tag_version="1.1"
    ):
        super(TagAnalysisMessage, self).__init__()
        self.original_tag = Tag.from_dict(tag_dict, as_version=tag_version)
        self.img_download_results = img_download_results
        self.img_analysis_results = img_analysis_results

    @staticmethod
    def from_json(json_msg):
        msg = json.loads(json_msg)
        return TagAnalysisMessage(
            json.loads(msg["original_tag"]),
            json.loads(msg["img_download_results"]),
            json.loads(msg["img_analysis_results"]),
        )

    def to_json(self):
        payload = {
            "original_tag": json.dumps(Tag.to_dict(self.original_tag)),
            "img_analysis_results": json.dumps(self.img_analysis_results),
            "img_download_results": json.dumps(self.img_download_results),
        }
        return json.dumps(payload, ensure_ascii=False)
