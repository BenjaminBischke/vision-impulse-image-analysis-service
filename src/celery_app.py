# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================


from __future__ import absolute_import, unicode_literals
from celery import Celery

app = Celery("floodnews_image_analysis")
app.config_from_object("celery_config")

if __name__ == "__main__":
    app.start()
