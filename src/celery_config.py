# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

from config import CELERY_BROKER_URL
from config import CELERY_TASK_SERIALIZER
from config import CELERY_RESULT_SERIALIZER
from config import CELERY_RESULT_PERSISTENT
from config import CELERY_ACCEPT_CONTENT
from config import CELERY_ENABLE_UTC
from config import CELERY_TIMEZONE
from config import CELERY_BROKER_DATA_FOLDER_OUT
from config import CELERY_BROKER_DATA_FOLDER_IN
from config import CELERY_BROKER_DATA_FOLDER_PROCESSED


task_serializer = CELERY_TASK_SERIALIZER
result_serializer = CELERY_RESULT_SERIALIZER
result_persistent = CELERY_RESULT_PERSISTENT
accept_content = CELERY_ACCEPT_CONTENT
timezone = CELERY_TIMEZONE
enable_utc = CELERY_ENABLE_UTC
broker_url = CELERY_BROKER_URL
broker_transport_options = {
    "data_folder_out": CELERY_BROKER_DATA_FOLDER_OUT,
    "data_folder_in": CELERY_BROKER_DATA_FOLDER_IN,
    "data_folder_processed": CELERY_BROKER_DATA_FOLDER_PROCESSED,
}
worker_concurrency=1

include = ["tasks.downloader_task", "tasks.image_analysis_task", "tasks.sender_task"]
