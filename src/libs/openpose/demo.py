import sys

sys.path.insert(0, "python")

import cv2
from libs.openpose import util
from libs.openpose.body import Body
import matplotlib.pyplot as plt
import copy
import shutil
import numpy as np
from operator import itemgetter

import os


def filter_images():
    folder = "/Users/Benni/Desktop/person_crops2"
    dst_folder = "/Users/Benni/Desktop/person_crops_filtered2"

    if not os.path.exists(dst_folder):
        os.makedirs(dst_folder)

    for fn in os.listdir(folder):
        if not fn.endswith(".jpg"):
            continue

        fp = os.path.join(folder, fn)
        b = os.path.getsize(fp)
        if b >= 6000:
            dst = os.path.join(dst_folder, fn)
            shutil.copy(fp, dst)


def run_folder():
    body_estimation = Body("/Users/Benni/Desktop/body_pose_model.pth")
    for fn in os.listdir("/Users/Benni/Desktop/person_crops"):
        if not fn.endswith(".jpg"):
            continue

        test_image = os.path.join("/Users/Benni/Desktop/person_crops", fn)
        print(test_image)

        # test_image = '/Users/Benni/Desktop/person_crops/1__849_3092.jpg'
        oriImg = cv2.imread(test_image)  # B,G,R order
        if oriImg is not None:
            try:
                candidate, subset = body_estimation(oriImg)
                canvas = copy.deepcopy(oriImg)
                canvas = util.draw_bodypose(canvas, candidate, subset)

                plt.imshow(canvas[:, :, [2, 1, 0]])
                plt.imsave(os.path.join("/Users/Benni/Desktop/openpose2/", fn), canvas)
            except ZeroDivisionError:
                continue


from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

test_image = "/Users/Benni/Desktop/MMSatAug/person_crops/0__190_13.jpg"


def extract_features_for_images():
    body_estimation = Body("/data/projects/floodnews/body_pose_model.pth")
    folder = "/data/projects/floodnews/african_dataset/person_crops_filtered"

    images = os.listdir(folder)  # [:100]

    X, Y = [], []
    for idx, fn in enumerate(images):
        label = 1 if fn.startswith("1") else 0
        vec = run_image_analysis(body_estimation, os.path.join(folder, fn))
        X.append([fn] + [vec])
        Y.append(np.array([label]))

    from sklearn.preprocessing import StandardScaler

    scaler = StandardScaler()

    X_train, X_test, y_train, y_test = train_test_split(
        X, Y, test_size=0.2, random_state=0
    )

    from sklearn.externals import joblib

    filename = "/data/projects/floodnews/openpose_svc.sav"
    logreg = joblib.load(filename)
    filename = "/data/projects/floodnews/openpose_scaler.sav"
    scaler = joblib.load(filename)

    X_test_imgs = [x[0] for x in X_test]
    X_train_imgs = [x[0] for x in X_train]
    X_test = [x[1] for x in X_test]
    X_train = [x[1] for x in X_train]
    """ 
    
    X_train = scaler.fit_transform(X_train)

    print("training classifier")
    #logreg = LogisticRegression()
    logreg = SVC(class_weight='balanced')
    logreg.fit(X_train, y_train)
    """

    X_test = scaler.transform(X_test)
    y_pred = logreg.predict(X_test)
    print(
        "Accuracy of logistic regression classifier on test set: {:.2f}".format(
            logreg.score(X_test, y_test)
        )
    )

    for x, y, img in zip(y_test, y_pred, X_test_imgs):
        #        print(x, y)
        if x != y:
            print(x, y, img)

    _confusion_matrix = confusion_matrix(y_test, y_pred)
    print(_confusion_matrix)
    print(classification_report(y_test, y_pred))

    from sklearn.externals import joblib

    filename = "/data/projects/floodnews/openpose_svc.sav"
    # joblib.dump(logreg, filename)
    filename = "/data/projects/floodnews/openpose_scaler.sav"
    # joblib.dump(scaler, filename)


def run_image_analysis(body_estimation, img_fp):
    oriImg = cv2.imread(img_fp)  # B,G,R order
    h, w, c = oriImg.shape
    candidate, subset = body_estimation(oriImg)
    num_persons = len(subset)

    op_features = []
    for person_idx in range(len(subset)):
        feature = []
        for p in subset[person_idx][:18]:
            if p == -1:
                val = np.array([-1.0, -1.0, -1.0,])
            else:
                val = np.array(
                    [
                        float(candidate[int(p)][0]) / w,
                        float(candidate[int(p)][1]) / h,
                        candidate[int(p)][2],
                    ]
                )
            feature.append(val)

        feature = list(np.array(feature).flatten())
        num_pose_points = subset[person_idx][18]

        if feature[4] == -1.0:
            x_position = 0
        else:
            x_position = abs(w / 2 - feature[4])

        op_features.append((x_position, num_pose_points, feature))

    # select the most centered person in case that there are more persons
    # visible on the cropped patch
    op_features = sorted(op_features, key=itemgetter(0))

    if len(subset) == 0:
        open_pose_vec = np.array([-1.0] * 54)  # 18 * 3
    else:
        open_pose_vec = np.array(op_features[0][2]).flatten()
    return open_pose_vec


def run_classification_analysis():
    body_estimation = Body(
        "/data/projects/floodnews/african_dataset/body_pose_model.pth"
    )
    folder = "/data/projects/floodnews/african_dataset/person_crops_filtered"

    prob = 0
    clear = 0
    num_flooding = 1
    images = os.listdir(folder)
    print(len(images))
    for fn in images:
        if fn.startswith("1") or not fn.endswith(".jpg"):
            continue
        num_flooding += 1

        test_image = os.path.join(folder, fn)
        oriImg = cv2.imread(test_image)  # B,G,R order
        candidate, subset = body_estimation(oriImg)
        # print("**"*30)
        # print(len(subset), fn)
        # print(subset)
        # print(candidate)
        # print(subset[0][10])
        # print(subset[0][13])
        found = False

        num_persons = len(subset)
        if len(subset) > 0:
            num_persons = 1

        foot_indices = []
        for i in range(num_persons):
            foot_indices.append(subset[i][10])
            foot_indices.append(subset[i][13])

        if len(foot_indices) == 0:
            continue

        for i in range(num_persons):
            if subset[i][10] == -1 or subset[i][13] == -1:
                print(fn)
                clear += 1
                found = True
                break

        # if not found:
        #    for f_idx in foot_indices:
        # print(f_idx)
        # print(candidate)
        # print(subset)
        # print(candidate[f_idx], f_idx)
        # if f_idx != -1 and candidate[int(f_idx)][2] < 50.:
        #    prob += 1
        #    break

    print(num_flooding)
    print(len(images), clear, prob)


extract_features_for_images()
# run_classification_analysis()#filter_images()
