# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# from libs.openpose import body
import libs.faster_rcnn._init_paths
import os
import sys
import numpy as np
import argparse
import pprint
import pdb
import time
import cv2
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim

import torchvision.transforms as transforms
import torchvision.datasets as dset

# from scipy.misc import imread
from libs.faster_rcnn.lib.roi_data_layer.roidb import combined_roidb
from libs.faster_rcnn.lib.roi_data_layer.roibatchLoader import roibatchLoader
from libs.faster_rcnn.lib.model.utils.config import (
    cfg,
    cfg_from_file,
    cfg_from_list,
    get_output_dir,
)
from libs.faster_rcnn.lib.model.rpn.bbox_transform import clip_boxes

# from model.nms.nms_wrapper import nms
from libs.faster_rcnn.lib.model.roi_layers import nms
from libs.faster_rcnn.lib.model.rpn.bbox_transform import bbox_transform_inv
from libs.faster_rcnn.lib.model.utils.net_utils import (
    save_net,
    load_net,
    vis_detections,
)
from libs.faster_rcnn.lib.model.utils.blob import im_list_to_blob
from libs.faster_rcnn.lib.model.faster_rcnn.vgg16 import vgg16
from libs.faster_rcnn.lib.model.faster_rcnn.resnet import resnet

import pdb
import json


PASCAL_CLASSES = np.asarray(
    [
        "__background__",
        "aeroplane",
        "bicycle",
        "bird",
        "boat",
        "bottle",
        "bus",
        "car",
        "cat",
        "chair",
        "cow",
        "diningtable",
        "dog",
        "horse",
        "motorbike",
        "person",
        "pottedplant",
        "sheep",
        "sofa",
        "train",
        "tvmonitor",
    ]
)


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)


class ObjectDetector(object):
    def __init__(self, model_fp, cfg_fp):
        load_name = model_fp
        cfg_file = cfg_fp
        cfg_from_file(cfg_file)

        self.class_agnostic = False
        self.cuda = torch.cuda.is_available()
        self.net = "res101"

        cfg.USE_GPU_NMS = self.cuda
        # print('Using config:')
        # pprint.pprint(cfg)
        np.random.seed(cfg.RNG_SEED)

        self.fasterRCNN = self._load_model(load_name)

        # initilize the tensor holder here.
        im_data = torch.FloatTensor(1)
        im_info = torch.FloatTensor(1)
        num_boxes = torch.LongTensor(1)
        gt_boxes = torch.FloatTensor(1)

        # ship to cuda
        if self.cuda > 0:
            im_data = im_data.cuda()
            im_info = im_info.cuda()
            num_boxes = num_boxes.cuda()
            gt_boxes = gt_boxes.cuda()

        # make variable
        self.im_data = Variable(im_data)
        self.im_info = Variable(im_info)
        self.num_boxes = Variable(num_boxes)
        self.gt_boxes = Variable(gt_boxes)

        if self.cuda > 0:
            cfg.CUDA = True

        if self.cuda > 0:
            self.fasterRCNN.cuda()
        self.fasterRCNN.eval()

        self.max_per_image = 100
        self.thresh = 0.05
        self.vis = False

    @staticmethod
    def save_predictions(filtered_dets, image_fn, out_folder):
        """ saves detect objects for the image to a jsonl file in the given out_folder
        """
        with open(os.path.join(out_folder, image_fn[:-4] + ".jsonl"), "w") as fp:
            for d in filtered_dets:
                fp.write(json.dumps(d, cls=MyEncoder) + "\n")

    @staticmethod
    def _get_image_blob(im):
        """Converts an image into a network input.
      Arguments:
        im (ndarray): a color image in BGR order
      Returns:
        blob (ndarray): a data blob holding an image pyramid
        im_scale_factors (list): list of image scales (relative to im) used
          in the image pyramid
      """
        im_orig = im.astype(np.float32, copy=True)
        im_orig -= cfg.PIXEL_MEANS

        im_shape = im_orig.shape
        im_size_min = np.min(im_shape[0:2])
        im_size_max = np.max(im_shape[0:2])

        processed_ims = []
        im_scale_factors = []

        for target_size in cfg.TEST.SCALES:
            im_scale = float(target_size) / float(im_size_min)
            # Prevent the biggest axis from being more than MAX_SIZE
            if np.round(im_scale * im_size_max) > cfg.TEST.MAX_SIZE:
                im_scale = float(cfg.TEST.MAX_SIZE) / float(im_size_max)
            im = cv2.resize(
                im_orig,
                None,
                None,
                fx=im_scale,
                fy=im_scale,
                interpolation=cv2.INTER_LINEAR,
            )
            im_scale_factors.append(im_scale)
            processed_ims.append(im)

        # Create a blob to hold the input images
        blob = im_list_to_blob(processed_ims)
        return blob, np.array(im_scale_factors)

    def _load_model(self, model_fp):
        # initilize the network here.
        if self.net == "res101":
            fasterRCNN = resnet(
                PASCAL_CLASSES,
                101,
                pretrained=False,
                class_agnostic=self.class_agnostic,
            )
        elif self.net == "res50":
            fasterRCNN = resnet(
                PASCAL_CLASSES, 50, pretrained=False, class_agnostic=self.class_agnostic
            )
        elif self.net == "res152":
            fasterRCNN = resnet(
                PASCAL_CLASSES,
                152,
                pretrained=False,
                class_agnostic=self.class_agnostic,
            )
        else:
            print("network is not defined")
            fasterRCNN = resnet(
                PASCAL_CLASSES,
                101,
                pretrained=False,
                class_agnostic=self.class_agnostic,
            )
            pdb.set_trace()
        fasterRCNN.create_architecture()

        # print("load checkpoint %s" % (model_fp))
        if self.cuda > 0:
            checkpoint = torch.load(model_fp)
        else:
            checkpoint = torch.load(
                model_fp, map_location=(lambda storage, loc: storage)
            )
        fasterRCNN.load_state_dict(checkpoint["model"])
        if "pooling_mode" in checkpoint.keys():
            cfg.POOLING_MODE = checkpoint["pooling_mode"]
        return fasterRCNN

    def analyze_folder(self, img_folder):
        # imglist = [fn for fn in os.listdir(self.img_folder) if not "det" in fn and not fn.endswith(".jsonl")]
        imglist = [
            fn
            for fn in os.listdir(img_folder)
            if fn.endswith("jpg") or fn.endswith("png")
        ]
        print("Loaded Photo: {} images.".format(len(imglist)))
        for image_fn in imglist:
            self.analyze_image(os.path.join(img_folder, image_fn))

    def analyze_image(self, image_fp):
        image_fn = os.path.basename(image_fp)
        # if os.path.exists(os.path.join(self.out_folder, image_fn[:-4] + ".jsonl")):
        #    return
        try:
            scores, pred_boxes, im = self._run_prediction(image_fp)
            filtered_dets = self._filter_pred_boxes(scores, pred_boxes, im, image_fn)
            scores, pred_boxes, im = None, None, None
            return filtered_dets
            # self.save_predictions(filtered_dets, image_fn, self.out_folder)
        except IndexError:
            return []
            # self.save_predictions([], image_fn, self.out_folder)
        except ValueError:
            return []
            # self.save_predictions([], image_fn, self.out_folder)
        except OSError:
            return []
            # self.save_predictions([], image_fn, self.out_folder)
            pass

    def _run_prediction(self, im_file):
        im_in = np.array(cv2.imread(im_file))

        if len(im_in.shape) == 2:
            im_in = im_in[:, :, np.newaxis]
            im_in = np.concatenate((im_in, im_in, im_in), axis=2)  #

        # rgb -> bgr
        # im = im_in[:, :, ::-1]
        im = im_in

        im_blob, im_scales = self._get_image_blob(im)
        assert len(im_scales) == 1, "Only single-image batch implemented"
        im_info_np = np.array(
            [[im_blob.shape[1], im_blob.shape[2], im_scales[0]]], dtype=np.float32
        )

        im_data_pt = torch.from_numpy(im_blob)
        im_data_pt = im_data_pt.permute(0, 3, 1, 2)
        im_info_pt = torch.from_numpy(im_info_np)

        # initilize the tensor holder here.
        im_data = torch.FloatTensor(1)
        im_info = torch.FloatTensor(1)
        num_boxes = torch.LongTensor(1)
        gt_boxes = torch.FloatTensor(1)

        # ship to cuda
        if self.cuda > 0:
            im_data = im_data.cuda()
            im_info = im_info.cuda()
            num_boxes = num_boxes.cuda()
            gt_boxes = gt_boxes.cuda()

        # make variable
        self.im_data = Variable(im_data)
        self.im_info = Variable(im_info)
        self.num_boxes = Variable(num_boxes)
        self.gt_boxes = Variable(gt_boxes)

        with torch.no_grad():
            self.im_data.resize_(im_data_pt.size()).copy_(im_data_pt)
            self.im_info.resize_(im_info_pt.size()).copy_(im_info_pt)
            self.gt_boxes.resize_(1, 1, 5).zero_()
            self.num_boxes.resize_(1).zero_()

            (
                rois,
                cls_prob,
                bbox_pred,
                rpn_loss_cls,
                rpn_loss_box,
                RCNN_loss_cls,
                RCNN_loss_bbox,
                rois_label,
            ) = self.fasterRCNN(
                self.im_data, self.im_info, self.gt_boxes, self.num_boxes
            )

            scores = cls_prob.data
            boxes = rois.data[:, :, 1:5]

            if cfg.TEST.BBOX_REG:
                # Apply bounding-box regression deltas
                box_deltas = bbox_pred.data
                if cfg.TRAIN.BBOX_NORMALIZE_TARGETS_PRECOMPUTED:
                    # Optionally normalize targets by a precomputed mean and stdev
                    if self.class_agnostic:
                        if self.cuda > 0:
                            box_deltas = (
                                box_deltas.view(-1, 4)
                                * torch.FloatTensor(
                                    cfg.TRAIN.BBOX_NORMALIZE_STDS
                                ).cuda()
                                + torch.FloatTensor(
                                    cfg.TRAIN.BBOX_NORMALIZE_MEANS
                                ).cuda()
                            )
                        else:
                            box_deltas = box_deltas.view(-1, 4) * torch.FloatTensor(
                                cfg.TRAIN.BBOX_NORMALIZE_STDS
                            ) + torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_MEANS)

                        box_deltas = box_deltas.view(1, -1, 4)
                    else:
                        if self.cuda > 0:
                            box_deltas = (
                                box_deltas.view(-1, 4)
                                * torch.FloatTensor(
                                    cfg.TRAIN.BBOX_NORMALIZE_STDS
                                ).cuda()
                                + torch.FloatTensor(
                                    cfg.TRAIN.BBOX_NORMALIZE_MEANS
                                ).cuda()
                            )
                        else:
                            box_deltas = box_deltas.view(-1, 4) * torch.FloatTensor(
                                cfg.TRAIN.BBOX_NORMALIZE_STDS
                            ) + torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_MEANS)
                        box_deltas = box_deltas.view(1, -1, 4 * len(PASCAL_CLASSES))

                pred_boxes = bbox_transform_inv(boxes, box_deltas, 1)
                pred_boxes = clip_boxes(pred_boxes, self.im_info.data, 1)
            else:
                # Simply repeat the boxes, once for each class
                pred_boxes = np.tile(boxes, (1, scores.shape[1]))

            pred_boxes /= im_scales[0]
            scores = scores.squeeze()
            pred_boxes = pred_boxes.squeeze()
            return scores, pred_boxes, im

    def _filter_pred_boxes(self, scores, pred_boxes, im, image_fn, thres=0.85):
        filtered_dets = []
        for j in range(1, len(PASCAL_CLASSES)):
            inds = torch.nonzero(scores[:, j] > self.thresh).view(-1)
            # if there is det
            if inds.numel() > 0:
                cls_scores = scores[:, j][inds]
                _, order = torch.sort(cls_scores, 0, True)
                if self.class_agnostic:
                    cls_boxes = pred_boxes[inds, :]
                else:
                    cls_boxes = pred_boxes[inds][:, j * 4 : (j + 1) * 4]

                cls_dets = torch.cat((cls_boxes, cls_scores.unsqueeze(1)), 1)
                # cls_dets = torch.cat((cls_boxes, cls_scores), 1)
                cls_dets = cls_dets[order]
                # keep = nms(cls_dets, cfg.TEST.NMS, force_cpu=not cfg.USE_GPU_NMS)
                keep = nms(cls_boxes[order, :], cls_scores[order], cfg.TEST.NMS)
                cls_dets = cls_dets[keep.view(-1).long()]
                if self.vis:
                    im2show = np.copy(im)
                    im2show = vis_detections(
                        im2show, PASCAL_CLASSES[j], cls_dets.cpu().numpy(), 0.5
                    )
                    # result_path = os.path.join(self.img_folder, image_fn[:-4] + "_" + PASCAL_CLASSES[j]
                    #                           + "_det.jpg")
                    # cv2.imwrite(result_path, im2show)
                _cls_dets = cls_dets.cpu().numpy()
                for cls_det in _cls_dets:
                    # print(cls_det, cls_det[4], PASCAL_CLASSES[j])
                    if cls_det[4] > thres:
                        filtered_dets.append(
                            {
                                "object_class": PASCAL_CLASSES[j],
                                "confidence": cls_det[4],
                                "bbox": list(cls_det[:4]),
                            }
                        )
        return filtered_dets
