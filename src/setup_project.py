# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions for setting up the environment of the project.
"""

import os
import yaml
import subprocess
from pathlib import Path
import inspect
import hashlib


def load_config(filedir, filename):
    """
    This function parses and loads the configuration files from Vision Impulse.


    :param filedir: directory in which the config file is located

    :param filename: filename of the config file

    :return: a dictionary with the config entries as key-value pairs
    """
    config_file_loc = Path(filedir, filename)
    if not config_file_loc.is_file():
        print("Can't find local config file:", config_file_loc)
        return
    with open(os.path.join(filedir, filename), "r", encoding="utf8") as configfile:
        config_data = yaml.safe_load(configfile)
    return config_data


def ensure_dir(path):
    """
    This function ensures that the given directory exists. If it does not exists, it is created.

    :param path: path to the directory
    """
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def checksum_md5(filepath):
    if os.path.exists(filepath):
        with open(filepath, 'rb') as obj:
            return hashlib.md5(obj.read()).hexdigest()
    return ""


def download_model(model_fn, model_dir, url, checksum, retry=5):
    model_path = os.path.join(model_dir, model_fn)
    for n in range(0, retry):
        if checksum_md5(model_path) == checksum:
            return
        subprocess.call(
            [
                "wget",
                "-P",
                model_dir,
                "-O",
                model_path,
                url,
            ]
        )
    if checksum_md5(model_path) != checksum:
        print("Model download failed (can not verify checksum):", model_fn)


PROJECT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
config_dir = os.path.join(PROJECT_DIR, "../data", "configs")
os.environ["CONFIG_DIR"] = config_dir


## floodtags folders
floodtags_config = load_config(config_dir, "floodtags_sdk.yaml")
for folder in floodtags_config["dir"]:
    ensure_dir(os.path.expanduser(floodtags_config["dir"][folder]))

## vision impulse folders
vi_config = load_config(config_dir, "vision_impulse.yaml")
ensure_dir(os.path.expanduser(vi_config["dir"]["model"]))
ensure_dir(os.path.expanduser(vi_config["dir"]["data"]))
ensure_dir(os.path.expanduser(vi_config["celery"]["broker_out_dir"]))
ensure_dir(os.path.expanduser(vi_config["celery"]["broker_in_dir"]))
ensure_dir(os.path.expanduser(vi_config["celery"]["broker_processed_dir"]))
ensure_dir(os.path.expanduser(vi_config["dir"]["db_backup"]))

model_dir = os.path.expanduser(vi_config["dir"]["model"])
model_ckecksums = vi_config["model_checksums"]

download_model("faster_rcnn_1_10_625.pth",
               model_dir,
               "http://vision-impulse.com/wb-models/faster_rcnn_1_10_625.pth",
               model_ckecksums["faster_rcnn_1_10_625.pth"])

download_model("flooding.model_best.pth.tar",
               model_dir,
               "http://vision-impulse.com/wb-models/flooding.model_best.pth.tar",
               model_ckecksums["flooding.model_best.pth.tar"])

download_model("body_pose_model.pth",
               model_dir,
               "http://vision-impulse.com/wb-models/body_pose_model.pth",
               model_ckecksums["body_pose_model.pth"])

download_model("openpose_scaler.sav",
               model_dir,
               "http://vision-impulse.com/wb-models/openpose_scaler.sav",
               model_ckecksums["openpose_scaler.sav"])

download_model("openpose_svc.sav",
               model_dir,
               "http://vision-impulse.com/wb-models/openpose_svc.sav",
               model_ckecksums["openpose_svc.sav"])

download_model("urbanization.model_best.pth.tar",
               model_dir,
               "http://vision-impulse.com/wb-models/urbanization.model_best.pth.tar",
               model_ckecksums["urbanization.model_best.pth.tar"])

download_model("rescuer.model_best.pth.tar",
               model_dir,
               "http://vision-impulse.com/wb-models/rescuer.model_best.pth.tar",
               model_ckecksums["rescuer.model_best.pth.tar"])

download_model("waterpatch.model_best.pth.tar",
               model_dir,
               "http://vision-impulse.com/wb-models/waterpatch.model_best.pth.tar",
               model_ckecksums["waterpatch.model_best.pth.tar"])
