# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================
"""
This module contains classes and functions of the service API endpoint of the image analysis.
"""

from __future__ import absolute_import, unicode_literals

import fastjsonschema

from floodtags_sdk.formats import Tag
from flask_restful import Resource, Api
from flask import Flask
from flask import request, jsonify

from celery_message import TagAnalysisMessage
from config import FLASK_HOST
from config import FLASK_DEBUG
from gevent.pywsgi import WSGIServer
import json
import os
from config import CELERY_BROKER_DATA_FOLDER_IN
from uuid import uuid4

from floodtags_sdk import logging

# This get's and configures the root logger for this script. Even though it's
# not called here, any loggers in packages and submodules will propagate to
# this and it makes sure Critical errors are logged before exit
logger = logging.get_logger('flood-image-analysis-service')


class ImageAnalysisService(Resource):
    """
    This class is responsible for the service endpoint of the image analysis.
    """

    def __init__(self):
        pass

    @staticmethod
    def _process_request(tag_dict):
        msg = TagAnalysisMessage(tag_dict, [], [])
        msg = msg.to_json()

        msg_id = str(uuid4())
        file_path = os.path.join(CELERY_BROKER_DATA_FOLDER_IN, msg_id + ".json")
        with open(file_path, "w") as fp:
            json.dump(msg, fp)

        file_path = os.path.join(CELERY_BROKER_DATA_FOLDER_IN, msg_id + ".trigger")
        with open(file_path, "w") as fp:
            pass

    def post(self):
        """
        This function processes HTTP requests that are send by the POST method to the service. The payload is
        stored on the file system.

        :return: the HTTP response code 200 if everything was successful and 400 in case of an error
        """
        tag_dict = request.get_json(force=True)
        if not isinstance(tag_dict, dict):
            response = jsonify({"message": "No valid json format received!",})
            response.status_code = 400
            return response
        try:
            # Parse it as a tag, and cast to dict again. This ensures data
            # version compatibility of the data format from FloodTags changes
            _tag = Tag.from_dict(tag_dict, as_version="1.1")
            tag_dict = _tag.to_dict(as_version="1.1")
            self._process_request(tag_dict)
        except fastjsonschema.exceptions.JsonSchemaException as ex:
            response = jsonify(
                {"message": "No valid tag object received (%s)!" % (ex.message),}
            )
            response.status_code = 400
            return response
        response = jsonify({"message": "Received"})
        response.status_code = 200
        return response


def create_app():
    """
    This function instantiates the flask app and attaches the service endpoint to "/image_api/".

    :return: the flask app object
    """
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(ImageAnalysisService, "/image_api/")
    return app


def main():
    """
    This function is the main entry point of this module to create the flask app and serve it as an endpoint through an wsgi web server.
    """
    app = create_app()

    if FLASK_DEBUG:
        app.run(debug=FLASK_DEBUG, host=FLASK_HOST)
    else:
        http_server = WSGIServer((FLASK_HOST, 5000), app)
        http_server.serve_forever()

# def start_offline():
#     import json
#
#     with open("tests/test_fixtures.json", "r") as fp:
#         data = json.load(fp)
#     tag_dict = data[0]
#
#     msg = TagAnalysisMessage(tag_dict, [], [])
#     msg = msg.to_json()


if __name__ == "__main__":
    main()
