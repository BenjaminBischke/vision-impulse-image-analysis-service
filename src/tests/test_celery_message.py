# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

import json
import unittest

from celery_message import TagAnalysisMessage


class TagAnalysisMessageTest(unittest.TestCase):
    """
        This class represents the test case for TagAnalysisMessage
    """

    def setUp(self):
        with open("./data/test_fixtures.json", "r") as fp:
            self.data = json.load(fp)

    def test_trigger_analysis__to_json(self):
        tag_dict = self.data[0]

        msg_obj = TagAnalysisMessage(tag_dict, [], [])
        msg_json = msg_obj.to_json()

        self.assertIsInstance(msg_json, str)
        self.assertIn("original_tag", msg_json)
        self.assertIn("img_download_results", msg_json)
        self.assertIn("img_analysis_results", msg_json)

        tag_str = json.dumps(tag_dict)
        msg = json.loads(msg_json)
        self.assertIn(tag_str, msg["original_tag"])

    def test_trigger_analysis__from_json(self):
        tag_dict = self.data[0]

        _json = json.dumps(
            {
                "original_tag": json.dumps(tag_dict),
                "img_download_results": ["image_path",],
                "img_analysis_results": [],
            }
        )

        tag_obj = TagAnalysisMessage.from_json(_json)

        self.assertIsInstance(tag_obj, TagAnalysisMessage)
        self.assertTrue(hasattr(tag_obj, "original_tag"))
        self.assertTrue(hasattr(tag_obj, "img_download_results"))
        self.assertTrue(hasattr(tag_obj, "img_analysis_results"))
        self.assertEqual(tag_obj.original_tag, tag_dict)
        self.assertEqual(tag_obj.img_download_results, ["image_path",])
        self.assertEqual(tag_obj.img_analysis_results, [])


if __name__ == "__main__":
    unittest.main()
