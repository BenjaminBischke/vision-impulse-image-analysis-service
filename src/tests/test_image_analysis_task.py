# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

import json
import unittest
from tasks.image_analysis_task import ImageAnalysisTask

# from service_api import create_app
from unittest.mock import patch


def mock_true(*args):
    return True


def mock_false(*args):
    return False


def mock_level(*args):
    return "low"


class ImageAnalysisTest(unittest.TestCase):
    """
        This class represents the test case for REST image analysis API
    """

    def setUp(self):
        self.analysis_task = ImageAnalysisTask()
        with open("./data/test_fixtures.json", "r") as fp:
            self.tag_data = json.load(fp)

        with open("./data/test_fixtures_download_result.json", "r") as fp:
            self.download_res = json.load(fp)

        with open("./data/test_fixtures_analysis_result.json", "r") as fp:
            self.analysis_res = json.load(fp)

        # '2018-09-10T12:34:56Z'
        self.timestamp_regex = (
            "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])"
            "T(2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]Z"
        )

    def test_process_images__no_image(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[0], [self.download_res[1]]
        )
        self.assertListEqual([{}], img_analysis)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_false,
    )
    def test_process_images__one_image__not_flood_related(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[0], [self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 1)
        single_analysis = img_analysis[0]
        self.assertRegex(single_analysis["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis)

        expected = {"floodRelated": False}
        single_analysis.pop("analysisTimestamp")
        single_analysis.pop("analysisVersion")

        self.assertDictEqual(expected, single_analysis)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_person_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_response_worker_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_cattle_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_non_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_water_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_building_present",
        mock_true,
    )
    @patch(
        "classifiers.water_level_estimation.WaterLevelEstimationClassifier.has_person_depth_evidence",
        mock_true,
    )
    @patch(
        "classifiers.urbanization_detection.UrbanizationDetectionClassifier.urbanization_level",
        mock_level,
    )
    def test_process_images__one_image__is_flood_related(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[0], [self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 1)

        single_analysis = img_analysis[0]
        self.assertIsInstance(single_analysis, dict)
        self.assertRegex(single_analysis["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis)

        expected = {
            "floodRelated": True,
            "cattlePresent": True,
            "transportMotorizedPresent": True,
            "transportWaterPresent": True,
            "transportNonMotorizedPresent": True,
            "buildingPresent": True,
            "personPresent": True,
            "urbanization": mock_level(),
            "personDepthEvidence": True,
            "responseWorkerPresent": True,
        }
        single_analysis.pop("analysisTimestamp")
        single_analysis.pop("analysisVersion")
        self.assertDictEqual(expected, single_analysis)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_person_present",
        mock_false,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_cattle_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_non_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_water_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_building_present",
        mock_true,
    )
    @patch(
        "classifiers.urbanization_detection.UrbanizationDetectionClassifier.urbanization_level",
        mock_level,
    )
    def test_process_images__one_image__flood_related__not_person_present(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[0], [self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 1)

        single_analysis = img_analysis[0]
        self.assertIsInstance(single_analysis, dict)
        self.assertRegex(single_analysis["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis)

        expected = {
            "floodRelated": True,
            "cattlePresent": True,
            "transportMotorizedPresent": True,
            "transportWaterPresent": True,
            "transportNonMotorizedPresent": True,
            "buildingPresent": True,
            "personPresent": False,
            "urbanization": mock_level(),
        }
        single_analysis.pop("analysisTimestamp")
        single_analysis.pop("analysisVersion")
        self.assertDictEqual(expected, single_analysis)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_person_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_response_worker_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_cattle_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_non_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_water_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_building_present",
        mock_false,
    )
    @patch(
        "classifiers.water_level_estimation.WaterLevelEstimationClassifier.has_person_depth_evidence",
        mock_true,
    )
    def test_process_images__one_image__flood_related__not_building_present(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[0], [self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 1)

        single_analysis = img_analysis[0]
        self.assertIsInstance(single_analysis, dict)
        self.assertRegex(single_analysis["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis)

        expected = {
            "floodRelated": True,
            "cattlePresent": True,
            "transportMotorizedPresent": True,
            "transportWaterPresent": True,
            "transportNonMotorizedPresent": True,
            "buildingPresent": False,
            "personPresent": True,
            "personDepthEvidence": True,
            "responseWorkerPresent": True,
        }
        single_analysis.pop("analysisTimestamp")
        single_analysis.pop("analysisVersion")
        self.assertDictEqual(expected, single_analysis)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_person_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_response_worker_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_cattle_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_non_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_water_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_building_present",
        mock_true,
    )
    @patch(
        "classifiers.water_level_estimation.WaterLevelEstimationClassifier.has_person_depth_evidence",
        mock_true,
    )
    @patch(
        "classifiers.urbanization_detection.UrbanizationDetectionClassifier.urbanization_level",
        mock_level,
    )
    def test_process_images__multiple_images__flood_related(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[2], [self.download_res[0], self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 2)

        expected = {
            "floodRelated": True,
            "cattlePresent": True,
            "transportMotorizedPresent": True,
            "transportWaterPresent": True,
            "transportNonMotorizedPresent": True,
            "buildingPresent": True,
            "personPresent": True,
            "urbanization": mock_level(),
            "personDepthEvidence": True,
            "responseWorkerPresent": True,
        }

        single_analysis1 = img_analysis[0]
        self.assertIsInstance(single_analysis1, dict)
        self.assertRegex(single_analysis1["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis1)
        single_analysis1.pop("analysisTimestamp")
        single_analysis1.pop("analysisVersion")
        self.assertDictEqual(expected, single_analysis1)

        single_analysis2 = img_analysis[1]
        self.assertIsInstance(single_analysis2, dict)
        self.assertRegex(single_analysis2["analysisTimestamp"], self.timestamp_regex)
        self.assertIn("analysisVersion", single_analysis2)
        single_analysis2.pop("analysisTimestamp")
        single_analysis2.pop("analysisVersion")
        self.assertDictEqual(expected, single_analysis2)

    @patch(
        "classifiers.flood_detection.FloodDetectionClassifier.is_flood_related",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_person_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_response_worker_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_cattle_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_non_motorized_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_water_transport_present",
        mock_true,
    )
    @patch(
        "classifiers.concept_detection.ConceptDetectionClassifier.is_building_present",
        mock_true,
    )
    @patch(
        "classifiers.water_level_estimation.WaterLevelEstimationClassifier.has_person_depth_evidence",
        mock_true,
    )
    @patch(
        "classifiers.urbanization_detection.UrbanizationDetectionClassifier.urbanization_level",
        mock_level,
    )
    def test_process_images__multiple_images_one_missing__flood_related(self):
        img_analysis = self.analysis_task.process_images(
            self.tag_data[2], [self.download_res[1], self.download_res[0]]
        )

        self.assertIsInstance(img_analysis, list)
        self.assertEqual(len(img_analysis), 2)

        single_analysis1 = img_analysis[0]
        self.assertDictEqual({}, single_analysis1)

        expected = {
            "floodRelated": True,
            "cattlePresent": True,
            "transportMotorizedPresent": True,
            "transportWaterPresent": True,
            "transportNonMotorizedPresent": True,
            "buildingPresent": True,
            "personPresent": True,
            "urbanization": mock_level(),
            "personDepthEvidence": True,
            "responseWorkerPresent": True,
        }
        single_analysis2 = img_analysis[1]
        self.assertIsInstance(single_analysis2, dict)
        self.assertIn("analysisVersion", single_analysis2)
        self.assertRegex(single_analysis2["analysisTimestamp"], self.timestamp_regex)
        single_analysis2.pop("analysisTimestamp")
        single_analysis2.pop("analysisVersion")

        self.assertDictEqual(expected, single_analysis2)


if __name__ == "__main__":
    unittest.main()
