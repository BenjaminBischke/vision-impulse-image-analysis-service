# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

import json
import unittest
from unittest.mock import patch
from celery_message import TagAnalysisMessage
from tasks.sender_task import ResultSenderTask


class SenderTaskTest(unittest.TestCase):
    """
        This class represents the test case for SenderTaskTest
    """

    def setUp(self):
        self.sender_task = ResultSenderTask()
        with open("./data/test_fixtures.json", "r") as fp:
            self.tag_data = json.load(fp)

        with open("./data/test_fixtures_download_result.json", "r") as fp:
            self.download_res = json.load(fp)

        with open("./data/test_fixtures_analysis_result.json", "r") as fp:
            self.analysis_res = json.load(fp)

        with open("./data/test_fixtures_sender_tag.json", "r") as fp:
            self.sender_sample = json.load(fp)

    def test_send_analysis_result__no_image(self):
        msg_obj = TagAnalysisMessage(self.tag_data[0], [self.download_res[1]], [{}])
        payload = self.sender_task._send_analysis_result(msg_obj)
        expected = self.sender_sample[2]
        self.assertDictEqual(expected, payload)

    def test_send_analysis_result__one_image(self):
        msg_obj = TagAnalysisMessage(
            self.tag_data[0], [self.download_res[0]], [self.analysis_res[0]]
        )

        payload = self.sender_task._send_analysis_result(msg_obj)
        expected = self.sender_sample[0]
        self.assertDictEqual(expected, payload)

    def test_send_analysis_result__multiple_images(self):
        msg_obj = TagAnalysisMessage(
            self.tag_data[2],
            [self.download_res[0], self.download_res[0]],
            [self.analysis_res[0], self.analysis_res[0]],
        )

        payload = self.sender_task._send_analysis_result(msg_obj)
        expected = self.sender_sample[3]
        self.assertDictEqual(expected, payload)

    def test_send_analysis_result__multiple_images__one_missing(self):
        msg_obj = TagAnalysisMessage(
            self.tag_data[2],
            [self.download_res[0], self.download_res[1]],
            [self.analysis_res[0], {}],
        )

        payload = self.sender_task._send_analysis_result(msg_obj)
        expected = self.sender_sample[1]
        self.assertDictEqual(expected, payload)


if __name__ == "__main__":
    unittest.main()
