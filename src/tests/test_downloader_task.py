# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

from unittest import TestCase

from tasks.downloader_task import ImageDownloaderTask
from tasks.downloader_task import ImageDownloaderException
import json
import os
import tempfile
from PIL import Image
import numpy as np


def mock_false(*args):
    return False


def mock_true(*args):
    return True


class DownloaderTaskTest(TestCase):
    def setUp(self):
        self.task = ImageDownloaderTask()
        with open("./data/test_fixtures.json", "r") as read_file:
            self.test_fixtures = json.load(read_file)
        self.temp_dir = os.path.join(tempfile.gettempdir(), "DownloaderTaskTest")
        if not os.path.exists(self.temp_dir):
            os.makedirs(self.temp_dir)

    def tearDown(self):
        self.task.database._close_connection_to_db()
        self.task = None
        if os.path.exists(self.temp_dir):
            os.rmdir(self.temp_dir)

    def test_image_path_from_url__normal_url(self):
        newspaper = "newspaper_source"
        img_url = "http://www.some-domain.com/article/image12345.jpg"
        image_path = self.task._image_path_from_url(
            newspaper, "2018", "123_1", img_url, root_folder=self.temp_dir
        )
        expected_path = os.path.join(self.temp_dir, "newspaper_source/2018/123_1.jpg")
        self.assertEqual(expected_path, image_path)

        img_url = "http://www.some-domain.com/article/image12345.png"
        image_path = self.task._image_path_from_url(
            newspaper, "2018", "123_1", img_url, root_folder=self.temp_dir
        )
        expected_path = os.path.join(self.temp_dir, "newspaper_source/2018/123_1.png")
        self.assertEqual(expected_path, image_path)

    def test_image_path_from_url__url_with_params(self):
        img_url = "http://www.some-domain.com/article/image12345.jpg?p=123"
        newspaper = "newspaper_source"
        image_path = self.task._image_path_from_url(
            newspaper, "2018", "123_1", img_url, root_folder=self.temp_dir
        )
        expected_path = os.path.join(self.temp_dir, "newspaper_source/2018/123_1.jpg")
        self.assertEqual(expected_path, image_path)

    def test_is_image_truncated__bad_image(self):
        self.assertTrue(self.task._is_image_truncated("./data/truncated.jpg"))

    def test_is_image_truncated__bad_file_path(self):
        self.assertTrue(self.task._is_image_truncated("./data/something.jpg"))

    def test_is_image_truncated__good_image(self):
        self.assertFalse(self.task._is_image_truncated("./data/valid.jpg"))

    def test_download_image__valid_image_url(self):
        img_url = "https://vision-impulse.com/wp-content/uploads/2019/06/logo_updated-e1560941558845.png"
        img_fp = os.path.join(self.temp_dir, "logo.png")
        self.task._download_image(img_url, img_fp)

        self.assertTrue(os.path.exists(img_fp))
        self.assertEqual(np.array(Image.open(img_fp)).shape, (90, 721, 4))

    def test_download_image__bad_image_url(self):
        img_url = (
            "https://vision-impulse.com/wp-content/uploads/2019/06/e1560941558845.png"
        )
        img_fp = os.path.join(self.temp_dir, "logo.png")

        self.assertRaises(
            ImageDownloaderException, self.task._download_image, img_url, img_fp
        )
