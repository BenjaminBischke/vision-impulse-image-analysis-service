# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

import unittest
from tasks.downloader_database import ImageDownloaderDatabase


class DownloaderDatabaseTest(unittest.TestCase):
    """
        This class represents the test case for DownloaderDatabase
    """

    def setUp(self):
        self.table_name = "image_downloader_test"
        self.database = ImageDownloaderDatabase()
        sql_stmt = """ CREATE TEMPORARY TABLE IF NOT EXISTS {table_name} (
            md5_url VARCHAR(64) KEY,
            sanitized_url TEXT NOT NULL, 
            original_url TEXT NOT NULL, 
            path TEXT NOT NULL 
        );"""
        sql_stmt = sql_stmt.format(table_name=self.table_name)
        self.database._execute_sql_stmt(sql_stmt)
        self.database.table_name = self.table_name

    def tearDown(self):
        sql_stmt = """ DROP TABLE {table_name} """
        sql_stmt = sql_stmt.format(table_name=self.table_name)
        self.database._execute_sql_stmt(sql_stmt)
        self.database._close_connection_to_db()

    def test_insert_url_and_image_path(self):
        self.database.insert_url_and_image_path("some_url.jpg?res=low", "some_path")

        sql_stmt = """SELECT md5_url, sanitized_url, original_url, path 
                    FROM {table_name} 
                    WHERE original_url = '{original_url}' """
        sql_stmt = sql_stmt.format(
            table_name=self.table_name, original_url="some_url.jpg?res=low"
        )
        res = self.database._execute_sql_stmt(sql_stmt)

        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0], "29322ec9d99968e987ebed71fd23d26c")
        self.assertEqual(res[0][1], "some_url.jpg")
        self.assertEqual(res[0][2], "some_url.jpg?res=low")
        self.assertEqual(res[0][3], "some_path")

    def test_select_image_path_for_url__empty(self):
        res = self.database.select_path_for_url("some_url")
        self.assertIsNone(res)

    def test_select_image_path_for_url__already_inserted(self):
        self.database.insert_url_and_image_path("some_url", "some_path")
        res = self.database.select_path_for_url("some_url")
        self.assertEqual(res, "some_path")

    def test_sanitize_url__clean_url(self):
        res = self.database._sanitize_url("http://www.fb/image.jpg")
        self.assertEqual(res, "http://www.fb/image.jpg")

    def test_sanitize_url__dirty_url(self):
        res = self.database._sanitize_url("http://www.fb/image.jpg?res=low")
        self.assertEqual(res, "http://www.fb/image.jpg")


if __name__ == "__main__":
    unittest.main()
