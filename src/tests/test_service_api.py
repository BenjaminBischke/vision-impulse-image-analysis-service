# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (C) 2019 Vision Impulse GmbH
# benjamin.bischke@vision-impulse.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ==============================================================================

import json
import unittest

from service_api import create_app
from unittest.mock import patch


def mock(*args):
    return "Trigger Analysis"


class RESTImageAnalysisAPITest(unittest.TestCase):
    """
        This class represents the test case for REST image analysis API
    """

    def setUp(self):
        self.app, api = create_app(config_name="testing")
        self.client = self.app.test_client
        with self.app.app_context():
            pass
        with open("./data/test_fixtures.json", "r") as fp:
            self.data = json.load(fp)

    @patch("service_api.ImageAnalysisService._process_request", mock)
    def test_trigger_analysis(self):
        payload = self.data[0]
        res = self.client().post(
            "/image_api/", data=json.dumps(payload), content_type="application/json"
        )
        self.assertEqual(res.status_code, 200)
        self.assertIn("received", str(res.data).lower())

    @patch("service_api.ImageAnalysisService._process_request", mock)
    def test_trigger_analysis__no_content_type(self):
        payload = self.data[0]
        res = self.client().post("/image_api/", data=json.dumps(payload))
        self.assertEqual(res.status_code, 200)
        self.assertIn("received", str(res.data).lower())

    @patch("service_api.ImageAnalysisService._process_request", mock)
    def test_trigger_analysis__corrupted_tag(self):
        payload = self.data[1]  # missing attribute
        res = self.client().post(
            "/image_api/", data=json.dumps(payload), content_type="application/json"
        )
        self.assertEqual(res.status_code, 400)
        self.assertIn("data must contain", str(res.data))

    @patch("service_api.ImageAnalysisService._process_request", mock)
    def test_trigger_analysis__invalid_json(self):
        payload = "some invalid json"
        res = self.client().post(
            "/image_api/", data=json.dumps(payload), content_type="application/json"
        )
        self.assertEqual(res.status_code, 400)
        self.assertIn("No valid json format received!", str(res.data))

    @patch("service_api.ImageAnalysisService._process_request", mock)
    def test_trigger_analysis__bad_url(self):
        payload = self.data[0]
        res = self.client().post(
            "/some_other_args/",
            data=json.dumps(payload),
            content_type="application/json",
        )
        self.assertEqual(res.status_code, 404)
        self.assertIn(
            "requested url was not found on the server.", str(res.data).lower()
        )


if __name__ == "__main__":
    unittest.main()
