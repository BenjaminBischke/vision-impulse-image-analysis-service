# Vision Impulse - Flood News Analysis System #

This repository contains the content for the FloodNews image analysis.


### What is this repository for? ###

* Details of classifiers will follow soon.


### How do I get set up? ###

Setup is simple, please clone the repository and run the bash script to install everything.
```
git clone https://BenjaminBischke@bitbucket.org/BenjaminBischke/vision-impulse-image-analysis-service.git
cd ./vision-impulse-image-analysis-service

sudo bash ./scripts/setup_ec2_instance.sh
```


### How do I start the analysis system ###

Start the flask server for the REST endpoint
```
sudo bash scripts/start_flask.sh
```

Start the worker process for the image analysis
```
sudo bash scripts/start_worker.sh
```


### How is the project structured ###

The project is structured in these folders:

* data
* scripts
* src

Configuration files are in these folders:

* data/configs/floodtags_sdk.yaml
* data/configs/vision_impulse.yaml


### Open points ###

* ~~cronjob for database dump and load on startup~~
* classifier performance
