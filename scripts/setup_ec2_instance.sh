#!/bin/bash
# ==============================================================================
#
# This script is used to setup the project
#
# Usage:
#   sh ./setup_ec2_instance.sh
# ==============================================================================
#
#
# ==============================================================================
# 1. Install software and required packages
sudo apt-get update && sudo apt-get install -y mariadb-server python3 python3-pip python3-opencv python-celery-common

pip3 install --no-cache-dir -r requirements.txt


# ==============================================================================
# 2. Setup the mysql database

/etc/init.d/mysql start
mysql -e "CREATE DATABASE IF NOT EXISTS image_analysis;"
mysql -e "CREATE USER IF NOT EXISTS 'image_analysis'@'%' IDENTIFIED BY 'some_pass';"
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'image_analysis'@'%' WITH GRANT OPTION;"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "CREATE TABLE IF NOT EXISTS image_analysis.image_downloader (md5_url text NOT NULL, sanitized_url text NOT NULL, original_url text NOT NULL, path text NOT NULL);"
mysql -e "CREATE TABLE IF NOT EXISTS image_analysis.image_duplicate_detection (md5_url text NOT NULL, image_hash text NOT NULL);"


# ==============================================================================
# 3. Setup the project (folder structure, pre-trained models)

export CONFIG_DIR=./data/configs
export PYTHONPATH=$PYTHONPATH:./src
export PYTHONPATH=$PYTHONPATH:./src/libs/faster_rcnn/lib

# 1. Set python path
python3 ./src/setup_project.py

# 2. Build Faster RCNN

cd ./src/libs/faster_rcnn/lib
python3 setup.py build develop


# ==============================================================================
# 4. Setup periodic database backup and load existing dump

cd ../../../../
db_name=$(niet ".database.db_name" $CONFIG_DIR/vision_impulse.yaml);
backup_dir=$(niet ".dir.db_backup" $CONFIG_DIR/vision_impulse.yaml);
backup_file="image_downloader_dump.sql"

if [[ $backup_dir = *~* ]]; then
  backup_dir=${backup_dir/\~/$HOME}
fi
sudo chown -R mysql: $backup_dir

if [ -f "$backup_dir/$backup_file" ]; then
    sudo mysql $db_name < "$backup_dir/$backup_file"
fi

sudo echo -e '#!/bin/bash\ncd '"${PWD}"' && sudo bash ./scripts/backup_db.sh '"${backup_dir}"'/'"${backup_file}"'' > /etc/cron.hourly/vision_impulse_backup_db
sudo chmod +x /etc/cron.hourly/vision_impulse_backup_db


# ==============================================================================
# 5. Setup periodic removing of old images (remove kept files if a given limit is exceeded)

sudo echo -e '#!/bin/bash\ncd '"${PWD}"' && sudo python3 ./src/image_removal.py' > /etc/cron.hourly/vision_impulse_remove_images
sudo chmod +x /etc/cron.hourly/vision_impulse_remove_images





