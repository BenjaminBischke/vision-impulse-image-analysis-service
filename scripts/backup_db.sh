#!/bin/bash
# ==============================================================================
#
# This script is used to backup the project database (executed periodically by cron job)
#
# Usage:
#   sh ./backup_db.sh target_dump_file.sql
# ==============================================================================
#
#
# ==============================================================================
# Save database backup as dump
if [ $(pgrep mysql | wc -l) -eq 1 ]; then
  config_dir=./data/configs
  db_user=$(niet ".database.username" $config_dir/vision_impulse.yaml);
  db_pwd=$(niet ".database.password" $config_dir/vision_impulse.yaml);
  db_name=$(niet ".database.db_name" $config_dir/vision_impulse.yaml);
  sudo mysqldump --quick --user="${db_user}" --password="${db_pwd}" $db_name > $1
fi