#!/bin/bash
# ==============================================================================
#
# This script is used to clean up project environment
# (delete database records, databse backups and celery out-files)
#
# Usage:
#   sh ./clean_environemnt.sh
# ==============================================================================
#
#
# ==============================================================================
#
/etc/init.d/mysql start
sudo mysql -e "DELETE FROM image_analysis.image_downloader;"
sudo mysql -e "DELETE FROM image_analysis.image_duplicate_detection;"

rm $(niet ".celery.broker_out_dir" ./data/configs/vision_impulse.yaml)/*
rm $(niet ".celery.broker_processed_dir" ./data/configs/vision_impulse.yaml)/*

rm $(niet ".dir.db_backup" ./data/configs/vision_impulse.yaml)/*